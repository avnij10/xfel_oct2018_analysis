import numpy as np
import h5py
import matplotlib.pyplot as plt
from cfelpyutils import crystfel_utils
from cfelpyutils import geometry_utils
import time

geomfilepath='/home/jaina/XFEL_test_agipd_data/geomfiles/agipd_fixed.geom'
path='/gpfs/exfel/data/scratch/reiserm/agipd_test/Data/AGIPD/XPCS_proposal_2013'
runno=45 #42, 45, 46, 47, 48, 49, 50, 51

## Experiment parameters (these parameters are not the ones used for this experiment. distance is < 5000)
energy=9.6 #keV
distance= 5000 #mm
pixL=0.2 #mm
adu_threshold = 80. # 0.7 x ADU_per_photon

geomfile = crystfel_utils.load_crystfel_geometry(geomfilepath)

#pixmap computes the pixelmaps in fp32; ouput is (x,y,r); shape is (512,2048)
pixmap = geometry_utils.compute_pix_maps(geomfile) 
assem = np.zeros(geometry_utils.compute_min_array_size(pixmap))

#vpixmap essentially rounds off the pixmap, so there is a chance some pixels at the edge of the asic are over-written
vpixmap=geometry_utils.compute_visualization_pix_maps(geomfile) 

####################################################################

firsttrainID=np.zeros(16)
pulseID_singleimage=np.zeros(16)
sumimage=np.zeros((512,2048))

count=0
fileno=0
changefileno=False

while True:
    if changefileno:
        fileno+=1
        changefileno=False
        if fileno==4:
            exit()

    # Comment: Loops over the files to check which first train ID to use
    for module in np.arange(0,16,1):
        filename_test='r'+str(runno).zfill(4)+'/CORR-R'+str(runno).zfill(4)+'-AGIPD'+str(module).zfill(2)+'-S'+str(fileno).zfill(5)+'.h5'
        print(filename_test)
        with h5py.File(path+'/'+filename_test) as file:
            trainIDpath='INSTRUMENT/SPB_DET_AGIPD1M-1/DET/'+str(module)+'CH0:xtdf/image/trainId' #shape is (16000,1)
            datapath='INSTRUMENT/SPB_DET_AGIPD1M-1/DET/'+str(module)+'CH0:xtdf/image/data' #shape is (16000,512,128)
            firsttrainID[module]=file[trainIDpath][0]
            nframes=file[trainIDpath].shape[0]

    firsttrainID_to_use=np.max(firsttrainID)

    # Comment: First loop: over the frames; Second loop: over each file, checks the first trainID, shifts if necessary, appends information of all modules into the array data
    for nf in np.arange(2,nframes+64,64):
        for module in np.arange(16):
            filename_test='r'+str(runno).zfill(4)+'/CORR-R'+str(runno).zfill(4)+'-AGIPD'+str(module).zfill(2)+'-S'+str(fileno).zfill(5)+'.h5'
            
            with h5py.File(path+'/'+filename_test) as file:
                trainIDpath='INSTRUMENT/SPB_DET_AGIPD1M-1/DET/'+str(module)+'CH0:xtdf/image/trainId' #shape is (16000,1)
                pulseIDpath='INSTRUMENT/SPB_DET_AGIPD1M-1/DET/'+str(module)+'CH0:xtdf/image/pulseId' #shape is (16000,1)
                datapath='INSTRUMENT/SPB_DET_AGIPD1M-1/DET/'+str(module)+'CH0:xtdf/image/data' #shape is (16000,512,128)

                #Comment
                trainID=file[trainIDpath][0:130] #slightly above 125
                pulseID=file[pulseIDpath]
                if len(np.argwhere(trainID==firsttrainID_to_use))==0:
                    arg_trainID_toshiftby=0
                else:
                    arg_trainID_toshiftby=np.argwhere(trainID==firsttrainID_to_use)[0][0]
                
                if (nf+arg_trainID_toshiftby)>=nframes:
                    changefileno=True
                    print(module,changefileno, nf+arg_trainID_toshiftby)
                else:
                    pulseID_singleimage[module]=pulseID[nf+arg_trainID_toshiftby]
                    vals = file[datapath][nf+arg_trainID_toshiftby][:]
                    vals[np.isnan(vals)]=0
                    vals[vals<adu_threshold] = 0
                    if module==0:
                        data=vals
                    else:
                        data=np.append(data,vals,axis=1)
                    
        ########### module loop over #################
        
        # Comment: Checks if the pulse ID of each module was the same or not.
        if not changefileno:    
            if len(set(pulseID_singleimage))!=1:
                print( "modules do not have the same pulse ID")

            count+=1
            sumimage+=data
            
            # Comment: prints the summed data after applying a threshold once every 100 frames
            if (count%100==0 and count > 0):
                
                assem[vpixmap.y, vpixmap.x] = sumimage
                np.save('assemimage_'+str(count).zfill(4)+'_'+str(fileno).zfill(4)+'.npy',assem)
                np.save('data_'+str(count).zfill(4)+'_'+str(fileno).zfill(4)+'.npy',sumimage)
                
                
